package app.lunaroja.superhero.ui.main.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import app.lunaroja.superhero.R
import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import app.lunaroja.superhero.databinding.FragmentHomeBinding
import app.lunaroja.superhero.ui.adapters.PrincipalCardAdapter
import app.lunaroja.superhero.ui.main.MainViewModel
import app.lunaroja.superhero.util.EventObserver
import app.lunaroja.superhero.util.NetworkConnection
import app.lunaroja.superhero.util.viewBinding

class HomeFragment : Fragment(R.layout.fragment_home) {

    private val viewModel by activityViewModels<MainViewModel>()
    private val binding by viewBinding(FragmentHomeBinding::bind)

    private lateinit var adapter: PrincipalCardAdapter
    private val listCharacters: MutableList<InfoBasicCharacter> = mutableListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        applyBinding()
        observeEvents()

        if (NetworkConnection.hasInternetConnection(activity)) {
            viewModel.callGetNameImageHero("1")
        }
    }

    private fun applyBinding() {
        initRecyclerView()
        with(binding) {
            rvCharacter.adapter = adapter
        }
    }

    private fun observeEvents() {
        viewModel.dataBasic.observe(viewLifecycleOwner, EventObserver {
            listCharacters.add(it)
            adapter.submitList(listCharacters)
            adapter.notifyDataSetChanged()

            if (NetworkConnection.hasInternetConnection(activity)) {
                val newIdCharacter = (listCharacters.size + 1).toString()
                viewModel.callGetNameImageHero(newIdCharacter)
            }

        })

        viewModel.dataBasicEnd.observe(viewLifecycleOwner, EventObserver {
            adapter.submitList(listCharacters)
            adapter.notifyDataSetChanged()
        })
    }

    private fun initRecyclerView() {
        adapter = PrincipalCardAdapter(
            listCharacters,
            actionClick = {
                findNavController().navigate(HomeFragmentDirections.toDetailCharacter(characterId = it))
            }
        )
    }
}