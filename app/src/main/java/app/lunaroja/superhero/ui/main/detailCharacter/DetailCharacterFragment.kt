package app.lunaroja.superhero.ui.main.detailCharacter

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.lunaroja.superhero.R
import app.lunaroja.superhero.databinding.FragmentDetailBinding
import app.lunaroja.superhero.ui.main.MainViewModel
import app.lunaroja.superhero.util.EventObserver
import app.lunaroja.superhero.util.NetworkConnection
import app.lunaroja.superhero.util.viewBinding

class DetailCharacterFragment : Fragment(R.layout.fragment_detail) {

    private val viewModel by activityViewModels<MainViewModel>()
    private val binding by viewBinding(FragmentDetailBinding::bind)
    private val args: DetailCharacterFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (NetworkConnection.hasInternetConnection(activity)) {
            viewModel.initGetDataCharacter(args.characterId)
        }

        applyBinding()
        observeEvents()

    }

    private fun applyBinding() {
        with(binding) {

        }
    }

    private fun observeEvents() {
        viewModel.powerstats.observe(viewLifecycleOwner, EventObserver {
            binding.tvPowerstatsCombat.text = setText(it.combat)
            binding.tvPowerstatsDurability.text = setText(it.durability)
            binding.tvPowerstatsIntelligence.text = setText(it.intelligence)
            binding.tvPowerstatsPower.text = setText(it.power)
            binding.tvPowerstatsSpeed.text = setText(it.speed)
            binding.tvPowerstatsStrength.text = setText(it.strength)
        })

        viewModel.biography.observe(viewLifecycleOwner, EventObserver {
            binding.tvBiographyAlignment.text = setText(it.alignment)
            binding.tvBiographyAlterEgos.text = setText(it.alterEgos)
            binding.tvBiographyFirstAppearance.text = setText(it.firstAppearance)
            binding.tvBiographyFullName.text = setText(it.fullName)
            binding.tvBiographyPlaceBirth.text = setText(it.placeBirth)
            binding.tvBiographyPublisher.text = setText(it.publisher)
            binding.tvBiographyAliases.text = getDataList(it.aliases)
        })

        viewModel.appearance.observe(viewLifecycleOwner, EventObserver {
            binding.tvAppearanceEyeColor.text = setText(it.eyeColor)
            binding.tvAppearanceGender.text = setText(it.gender)
            binding.tvAppearanceHairColor.text = setText(it.hairColor)
            binding.tvAppearanceRace.text = setText(it.race)
            binding.tvAppearanceHeight.text = getDataList(it.height)
            binding.tvAppearanceWeight.text = getDataList(it.weight)
        })

        viewModel.work.observe(viewLifecycleOwner, EventObserver {
            binding.tvWorkOccupation.text = setText(it.occupation)
            binding.tvWorkBaseOperation.text = setText(it.base)
        })

        viewModel.connections.observe(viewLifecycleOwner, EventObserver {
            binding.tvConnectionsGroupAffiliation.text = setText(it.groupAffiliation)
            binding.tvConnectionsRelatives.text = setText(it.relatives)
        })
    }

    private fun setText(text: String): String {
        return when (text == "null" || text == "-" || text.isEmpty()) {
            true -> "Sin Info"
            false -> text
        }
    }

    private fun getDataList(listString: List<String>): String {
        var result = ""
        val listIterator = listString.listIterator()
        while (listIterator.hasNext()) {
            val temp = listIterator.next()
            if (!temp.contains("-0")) {
                result = temp
            }
        }
        return when (result.isEmpty()) {
            true -> "Sin Info"
            false -> result
        }
    }

}