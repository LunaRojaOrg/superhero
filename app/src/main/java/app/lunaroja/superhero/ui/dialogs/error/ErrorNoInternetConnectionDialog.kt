package app.lunaroja.superhero.ui.dialogs.error

import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.FragmentActivity
import app.lunaroja.superhero.databinding.DialogNoInternetConnectionBinding

class ErrorNoInternetConnectionDialog(activity: FragmentActivity) : AppCompatDialog(activity) {

    private var actionDismiss: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        val binding = DialogNoInternetConnectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAccept.setOnClickListener {
            dismiss()
            actionDismiss?.invoke()
        }
    }

    override fun onStart() {
        super.onStart()
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        window?.apply {
            setLayout(width, height)
            setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    class Builder(activity: FragmentActivity) {
        private val dialog = ErrorNoInternetConnectionDialog(activity)

        fun withDismissCallback(actionDismiss: () -> Unit): Builder {
            this.dialog.actionDismiss = actionDismiss
            return this
        }

        fun show() {
            dialog.show()
        }
    }
}