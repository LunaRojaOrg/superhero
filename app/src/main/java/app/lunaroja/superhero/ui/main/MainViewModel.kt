package app.lunaroja.superhero.ui.main

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import app.lunaroja.superhero.base.BaseViewModel
import app.lunaroja.superhero.data.model.service.response.*
import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import app.lunaroja.superhero.util.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    private val homeRepository: HomeRepository,
    private val detailCharacterRepository: DetailCharacterRepository,
    private val context: Context,
    state: SavedStateHandle
) : BaseViewModel() {

    private val disposable = CompositeDisposable()

    private val _dataBasic = MutableLiveData<Event<InfoBasicCharacter>>()
    val dataBasic get() = _dataBasic

    private val _dataBasicEnd = MutableLiveData<Event<String>>()
    val dataBasicEnd get() = _dataBasicEnd

    private val _powerstats = MutableLiveData<Event<PowerStatsResponse>>()
    val powerstats get() = _powerstats

    private val _biography = MutableLiveData<Event<BiographyResponse>>()
    val biography get() = _biography

    private val _appearance = MutableLiveData<Event<AppearanceResponse>>()
    val appearance get() = _appearance

    private val _work = MutableLiveData<Event<WorkResponse>>()
    val work get() = _work

    private val _connections = MutableLiveData<Event<ConnectionsResponse>>()
    val connections get() = _connections


    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

    fun initGetDataCharacter(characterId: String) {
        CoroutineScope(Dispatchers.IO).launch {
            callGetPowerStatsHero(characterId)
            callGetBiographyHero(characterId)
            callGetAppearanceHero(characterId)
            callGetWorkHero(characterId)
            callGetConnectionsHero(characterId)
        }
    }

    fun callGetNameImageHero(characterId: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val task = homeRepository.callGetNameImageHero(characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
            val subscriber = task.subscribe({
                _dataBasic.value = Event(it)
            }, {
                _dataBasicEnd.value = Event("Ya no hay mas Heroes ${it.message}")
            })
            disposable.add(subscriber)
        }
    }

    fun callGetPowerStatsHero(characterId: String) {
        val task = detailCharacterRepository.callGetPowerStatsHero(characterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val subscriber = task.subscribe({
            _powerstats.value = Event(it)
        }, {
            serviceError(it.message!!)
        })
        disposable.add(subscriber)
    }

    fun callGetBiographyHero(characterId: String) {
        val task = detailCharacterRepository.callGetBiographyHero(characterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val subscriber = task.subscribe({
            _biography.value = Event(it)
        }, {
            serviceError(it.message!!)
        })
        disposable.add(subscriber)
    }

    fun callGetAppearanceHero(characterId: String) {
        val task = detailCharacterRepository.callGetAppearanceHero(characterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val subscriber = task.subscribe({
            _appearance.value = Event(it)
        }, {
            serviceError(it.message!!)
        })
        disposable.add(subscriber)
    }

    fun callGetWorkHero(characterId: String) {
        val task = detailCharacterRepository.callGetWorkHero(characterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val subscriber = task.subscribe({
            _work.value = Event(it)
        }, {
            serviceError(it.message!!)
        })
        disposable.add(subscriber)
    }

    fun callGetConnectionsHero(characterId: String) {
        val task = detailCharacterRepository.callGetConnectionsHero(characterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        val subscriber = task.subscribe({
            _connections.value = Event(it)
        }, {
            serviceError(it.message!!)
        })
        disposable.add(subscriber)
    }

}