package app.lunaroja.superhero.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import app.lunaroja.superhero.databinding.ItemListPrincipalCharacterBinding
import app.lunaroja.superhero.util.setOnSingleClickListener
import com.bumptech.glide.Glide

class PrincipalCardAdapter(
    private val items: List<InfoBasicCharacter>,
    private val actionClick: (String) -> Unit
) : ListAdapter<InfoBasicCharacter, RecyclerView.ViewHolder>(CardDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrincipalCardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListPrincipalCharacterBinding.inflate(inflater, parent, false)
        return PrincipalCardViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val infoModel = getItem(position)
        if (holder is PrincipalCardViewHolder) {
            holder.bind(infoModel)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class PrincipalCardViewHolder(val binding: ViewBinding, val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(cardModel: InfoBasicCharacter) {

            with(binding as ItemListPrincipalCharacterBinding) {

                Glide.with(context)
                    .load(cardModel.image)
                    .into(ivCharacter)

                tvNameCharacter.text = cardModel.name

                rlMainCharacter.setOnSingleClickListener {
                    actionClick(cardModel.id)
                }

            }
        }

    }

    object CardDiff : DiffUtil.ItemCallback<InfoBasicCharacter>() {
        override fun areItemsTheSame(
            oldItem: InfoBasicCharacter,
            newItem: InfoBasicCharacter
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: InfoBasicCharacter,
            newItem: InfoBasicCharacter
        ): Boolean {
            return oldItem == newItem
        }
    }

}