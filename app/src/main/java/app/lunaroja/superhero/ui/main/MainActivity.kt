package app.lunaroja.superhero.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import app.lunaroja.superhero.AppAplication
import app.lunaroja.superhero.R
import app.lunaroja.superhero.base.BaseViewModelActivity
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import app.lunaroja.superhero.databinding.ActivityMainBinding
import app.lunaroja.superhero.di.component.inject.DaggerMainComponent
import app.lunaroja.superhero.di.factory.MainViewModelFactory
import javax.inject.Inject

class MainActivity : BaseViewModelActivity<MainViewModel>() {

    @Inject
    lateinit var homeRepository: HomeRepository

    @Inject
    lateinit var detailCharacterRepository: DetailCharacterRepository

    private val homeViewModel by viewModels<MainViewModel> {
        MainViewModelFactory(
            homeRepository,
            detailCharacterRepository,
            this,
            this,
            intent.extras
        )
    }

    override val baseViewModel: MainViewModel
        get() = homeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        initInjection()
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater).apply {
            lifecycleOwner = this@MainActivity
        }

        setContentView(binding.root)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorAccent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initInjection() {
        val component = DaggerMainComponent.builder()
            .applicationComponent(AppAplication.applicationComponent)
            .build()

        component.inject(this)
    }

}