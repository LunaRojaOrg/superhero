package app.lunaroja.superhero

import android.app.Application
import app.lunaroja.superhero.data.module.ApplicationModule
import app.lunaroja.superhero.data.module.DataSourceModule
import app.lunaroja.superhero.data.module.NetworkModule
import app.lunaroja.superhero.di.component.ApplicationComponent
import app.lunaroja.superhero.di.component.DaggerApplicationComponent

class AppAplication : Application() {

    companion object {
        var applicationComponent: ApplicationComponent? = null
            private set

        init {
            System.loadLibrary("secure")
        }
    }

    private external fun tokenAccessFromJNI(): String

    override fun onCreate() {
        super.onCreate()
        configureDagger()
    }

    private fun configureDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(applicationContext))
            .dataSourceModule(DataSourceModule())
            .networkModule(
                NetworkModule(
                    getBaseUrl()
                )
            )
            .build()
    }

    private fun getBaseUrl(): String {
        return applicationContext.getString(R.string.base_url).plus(tokenAccessFromJNI()).plus("/")
    }

}