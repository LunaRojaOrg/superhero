package app.lunaroja.superhero.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.lunaroja.superhero.util.Event
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    protected val baseDisposable = CompositeDisposable()

    private val _serviceError = MutableLiveData<Event<String?>>()
    val serviceError get() = _serviceError

    private val _connectionError = MutableLiveData<Event<String?>>()
    val connectionError get() = _connectionError

    protected fun serviceError(message: String?) {
        _serviceError.postValue(Event(message))
    }

    protected fun connectionError(message: String?) {
        _connectionError.postValue(Event(message))
    }

    override fun onCleared() {
        baseDisposable.dispose()
        super.onCleared()
    }

}