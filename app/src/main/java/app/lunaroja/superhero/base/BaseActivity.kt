package app.lunaroja.superhero.base

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import app.lunaroja.superhero.util.hideKeyboard
import com.google.android.material.textfield.TextInputEditText

abstract class BaseActivity : AppCompatActivity() {

    private val displayMetrics = DisplayMetrics()

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val ret = super.dispatchTouchEvent(event)
        if (currentFocus is TextInputEditText) {
            val view = currentFocus
            view?.let {
                val coordinates = IntArray(2)
                view.getLocationOnScreen(coordinates)
                val x = event.rawX + view.left - coordinates[0]
                val y = event.rawY + view.top - coordinates[1]
                if (event.action == MotionEvent.ACTION_UP &&
                    (x < view.left || x >= view.right || y < view.top || y > view.bottom)
                ) {
                    hideKeyboard(view)
                }
            }
        }
        return ret
    }

    override fun attachBaseContext(newBase: Context?) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val var10000 = newBase?.getSystemService(WINDOW_SERVICE)
                if (var10000 == null) {
                    throw ClassCastException("null cannot be cast to non-null type android.view.WindowManager")
                } else {
                    val windowManager = var10000 as WindowManager
                    windowManager.defaultDisplay.getMetrics(this.displayMetrics)
                    if (this.displayMetrics.densityDpi != this.displayMetrics.xdpi.toInt()) {
                        val var10002 = newBase.resources
                        val newConfiguration = Configuration(var10002?.configuration)
                        newConfiguration.densityDpi = this.displayMetrics.xdpi.toInt()
                        newConfiguration.fontScale = 1.1f
                        applyOverrideConfiguration(newConfiguration)
                    }
                }
            }
        } catch (ignore: Exception) {

        }
        super.attachBaseContext(newBase)
    }
}