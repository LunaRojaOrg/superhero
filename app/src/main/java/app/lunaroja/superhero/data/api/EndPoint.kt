package app.lunaroja.superhero.data.api

object EndPoint {
    internal const val NAME_IMAGE_HERO = "{characterId}/image"
    internal const val POWER_STATS_HERO = "{characterId}/powerstats"
    internal const val BIOGRAPHY_HERO = "{characterId}/biography"
    internal const val APPEARANCE_HERO = "{characterId}/appearance"
    internal const val WORK_HERO = "{characterId}/work"
    internal const val CONNECTIONS_HERO = "{characterId}/connections"
}