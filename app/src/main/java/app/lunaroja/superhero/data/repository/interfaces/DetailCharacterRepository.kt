package app.lunaroja.superhero.data.repository.interfaces

import app.lunaroja.superhero.data.model.service.response.*
import io.reactivex.Single

interface DetailCharacterRepository {

    fun callGetPowerStatsHero(characterId: String): Single<PowerStatsResponse>

    fun callGetBiographyHero(characterId: String): Single<BiographyResponse>

    fun callGetAppearanceHero(characterId: String): Single<AppearanceResponse>

    fun callGetWorkHero(characterId: String): Single<WorkResponse>

    fun callGetConnectionsHero(characterId: String): Single<ConnectionsResponse>

}