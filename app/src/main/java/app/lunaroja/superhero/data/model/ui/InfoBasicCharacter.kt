package app.lunaroja.superhero.data.model.ui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InfoBasicCharacter (
    val id: String,
    val name: String,
    val image: String
) : Parcelable {

}