package app.lunaroja.superhero.data.repository.classes

import android.util.Log
import app.lunaroja.superhero.data.api.ServiceApi
import app.lunaroja.superhero.data.mappers.DataMapper
import app.lunaroja.superhero.data.model.service.response.NameImageResponse
import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import app.lunaroja.superhero.data.network.HandleServiceError
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitHomeRepository
@Inject constructor(
    private val apiService: ServiceApi
) : HandleServiceError(), HomeRepository {

    override fun callGetNameImageHero(characterId: String): Single<InfoBasicCharacter> {
        return apiService.getNameImageHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                val response = serviceResponse.body()?.let { DataMapper().mapFrom(it) }
                return@map response
            }
    }
}