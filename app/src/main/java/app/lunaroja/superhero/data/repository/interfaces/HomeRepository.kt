package app.lunaroja.superhero.data.repository.interfaces

import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import io.reactivex.Single

interface HomeRepository {

    fun callGetNameImageHero(characterId: String): Single<InfoBasicCharacter>

}