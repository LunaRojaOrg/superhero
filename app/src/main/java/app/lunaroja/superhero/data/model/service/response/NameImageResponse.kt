package app.lunaroja.superhero.data.model.service.response

import com.google.gson.annotations.SerializedName

data class NameImageResponse(
    @SerializedName("response")
    val response: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String
)
