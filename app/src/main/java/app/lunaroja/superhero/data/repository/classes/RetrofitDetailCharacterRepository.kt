package app.lunaroja.superhero.data.repository.classes

import app.lunaroja.superhero.data.api.ServiceApi
import app.lunaroja.superhero.data.model.service.response.*
import app.lunaroja.superhero.data.network.HandleServiceError
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitDetailCharacterRepository
@Inject constructor(
    private val apiService: ServiceApi
) : HandleServiceError(), DetailCharacterRepository {

    override fun callGetPowerStatsHero(characterId: String): Single<PowerStatsResponse> {
        return apiService.getPowerStatsHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                return@map serviceResponse.body()
            }
    }

    override fun callGetBiographyHero(characterId: String): Single<BiographyResponse> {
        return apiService.getBiographyHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                return@map serviceResponse.body()
            }
    }

    override fun callGetAppearanceHero(characterId: String): Single<AppearanceResponse> {
        return apiService.getAppearanceHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                return@map serviceResponse.body()
            }
    }

    override fun callGetWorkHero(characterId: String): Single<WorkResponse> {
        return apiService.getWorkHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                return@map serviceResponse.body()
            }
    }

    override fun callGetConnectionsHero(characterId: String): Single<ConnectionsResponse> {
        return apiService.getConnectionsHero(characterId)
            .map { serviceResponse ->
                handleResponse(serviceResponse)
                return@map serviceResponse.body()
            }
    }

}