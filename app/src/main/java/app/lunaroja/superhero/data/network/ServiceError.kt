package app.lunaroja.superhero.data.network

import com.google.gson.annotations.SerializedName
import java.net.HttpURLConnection

data class ServiceError(
    @SerializedName("response")
    val response: String,

    @SerializedName("error")
    val error: String
)