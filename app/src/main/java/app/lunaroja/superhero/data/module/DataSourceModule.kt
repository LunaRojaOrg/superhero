package app.lunaroja.superhero.data.module

import app.lunaroja.superhero.data.repository.classes.RetrofitDetailCharacterRepository
import app.lunaroja.superhero.data.repository.classes.RetrofitHomeRepository
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataSourceModule {

    @Singleton
    @Provides
    fun provideMainRepository(repository: RetrofitHomeRepository): HomeRepository {
        return repository
    }

    @Singleton
    @Provides
    fun provideDetailCharacterRepository(repository: RetrofitDetailCharacterRepository): DetailCharacterRepository {
        return repository;
    }

}