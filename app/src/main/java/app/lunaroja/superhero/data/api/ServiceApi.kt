package app.lunaroja.superhero.data.api

import app.lunaroja.superhero.data.model.service.response.*
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface ServiceApi {

    @GET(EndPoint.NAME_IMAGE_HERO)
    fun getNameImageHero(
        @Path("characterId") characterId: String
    ): Single<Response<NameImageResponse>>

    @GET(EndPoint.POWER_STATS_HERO)
    fun getPowerStatsHero(
        @Path("characterId") characterId: String
    ): Single<Response<PowerStatsResponse>>

    @GET(EndPoint.BIOGRAPHY_HERO)
    fun getBiographyHero(
        @Path("characterId") characterId: String
    ): Single<Response<BiographyResponse>>

    @GET(EndPoint.APPEARANCE_HERO)
    fun getAppearanceHero(
        @Path("characterId") characterId: String
    ): Single<Response<AppearanceResponse>>

    @GET(EndPoint.WORK_HERO)
    fun getWorkHero(
        @Path("characterId") characterId: String
    ): Single<Response<WorkResponse>>

    @GET(EndPoint.CONNECTIONS_HERO)
    fun getConnectionsHero(
        @Path("characterId") characterId: String
    ): Single<Response<ConnectionsResponse>>

}