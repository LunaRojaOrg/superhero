package app.lunaroja.superhero.data.mappers

import io.reactivex.Single

abstract class Mapper<in I, O> {

    abstract fun mapFrom(from: I): O

    fun mapOptional(from: Optional<I>): Optional<O> {
        from.value?.let {
            return Optional.of(mapFrom(it))
        } ?: return Optional.empty()
    }

    fun observable(from: I): Single<O> {
        return Single.fromCallable { mapFrom(from) }
    }

    fun observable(from: List<I>): Single<List<O>> {
        return Single.fromCallable { from.map { mapFrom(it) } }
    }
}