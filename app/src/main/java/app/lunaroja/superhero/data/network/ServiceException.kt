package app.lunaroja.superhero.data.network

class ServiceException(
    val response: String? = "",
    val error: String? = "",
    override val message: String
) : RuntimeException(message)