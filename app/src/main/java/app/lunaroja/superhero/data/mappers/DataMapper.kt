package app.lunaroja.superhero.data.mappers

import app.lunaroja.superhero.data.model.service.response.NameImageResponse
import app.lunaroja.superhero.data.model.ui.InfoBasicCharacter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataMapper
@Inject
constructor() : Mapper<NameImageResponse, InfoBasicCharacter>() {
    override fun mapFrom(from: NameImageResponse): InfoBasicCharacter {
        return InfoBasicCharacter(
            id = from.id,
            name = from.name,
            image = from.url
        )
    }
}