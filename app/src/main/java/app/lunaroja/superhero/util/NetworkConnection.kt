package app.lunaroja.superhero.util

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import app.lunaroja.superhero.ui.dialogs.error.ErrorNoInternetConnectionDialog

object NetworkConnection {

    @RequiresApi(Build.VERSION_CODES.M)
    private fun isConnected(activity: Activity?): Boolean {
        activity?.let {
            val manager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val activeNetwork = manager?.activeNetwork ?: return false
            val networkCapabilities = manager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        }
        return false
    }

    fun hasInternetConnection(activity: FragmentActivity?): Boolean {
        activity?.let {
            return if (isConnected(it)) {
                true
            } else {
                ErrorNoInternetConnectionDialog.Builder(it)
                    .show()
                return false
            }
        }
        return false
    }
}