package app.lunaroja.superhero.di.factory

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import app.lunaroja.superhero.ui.main.MainViewModel

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val homeRepository: HomeRepository,
    private val detailCharacterRepository: DetailCharacterRepository,
    private val context: Context,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle?
) :
    AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    override fun <T : ViewModel?> create(key: String, clazz: Class<T>, state: SavedStateHandle): T {
        val viewModel: ViewModel = if (clazz == MainViewModel::class.java) {
            MainViewModel(
                homeRepository,
                detailCharacterRepository,
                context,
                state
            )
        } else {
            throw RuntimeException("Unsupported view model class: $clazz")
        }
        return viewModel as T
    }
}