package app.lunaroja.superhero.di.component

import app.lunaroja.superhero.data.module.ApplicationModule
import app.lunaroja.superhero.data.module.DataSourceModule
import app.lunaroja.superhero.data.module.NetworkModule
import app.lunaroja.superhero.data.repository.interfaces.DetailCharacterRepository
import app.lunaroja.superhero.data.repository.interfaces.HomeRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, DataSourceModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun homeRepository(): HomeRepository

    fun detailCharacterRepository(): DetailCharacterRepository

}