package app.lunaroja.superhero.di.component.inject

import app.lunaroja.superhero.di.component.ApplicationComponent
import app.lunaroja.superhero.di.scope.FragmentScope
import app.lunaroja.superhero.ui.main.MainActivity
import dagger.Component

@FragmentScope
@Component(dependencies = [ApplicationComponent::class])
interface MainComponent {

    fun inject(target: MainActivity)

}