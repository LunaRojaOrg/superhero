package app.lunaroja.superhero.di.navigation

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment

/**
 * To be implemented by components that host top-level navigation destinations.
 */
interface NavigationHost {

    /** Called by MainNavigationFragment to setup it's toolbar with the navigation controller. */
    fun registerToolbarWithNavigation(title: String)

    fun showBackToolbar(visibility: Int) {

    }


    fun toolbarBackground(drawable: Drawable?) {
    }
}

/**
 * To be implemented by main navigation destinations shown by a [NavigationHost].
 */
interface NavigationDestination {

    /** Called by the host when the user interacts with it. */
    fun onUserInteraction() {}
}

/**
 * Fragment representing a main navigation destination. This class handles wiring up the [Toolbar]
 * navigation icon if the fragment is attached to a [NavigationHost].
 */
open class MainNavigationFragment(contentLayoutId: Int) : Fragment(contentLayoutId),
    NavigationDestination {

    protected var navigationHost: NavigationHost? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationHost) {
            navigationHost = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        navigationHost = null
    }
}
